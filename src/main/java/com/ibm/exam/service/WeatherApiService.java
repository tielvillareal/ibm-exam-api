package com.ibm.exam.service;

import com.ibm.exam.model.WeatherData;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherApiService {

    public WeatherData getWeatherData(String city) {
        RestTemplate restTemplate = new RestTemplate();

        String resourceUrl = "https://api.openweathermap.org/data/2.5/weather?q=" +
                city + "&APPID=f67633a0f78299d122b73fa3348ba30d";

        return restTemplate.getForObject(resourceUrl, WeatherData.class);
    }
}
