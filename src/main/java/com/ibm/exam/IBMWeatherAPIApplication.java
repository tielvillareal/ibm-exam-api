package com.ibm.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IBMWeatherAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(IBMWeatherAPIApplication.class, args);
    }
}
