package com.ibm.exam.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown =  true)
public class WeatherData implements Serializable{

    private int id;
    private String location;
    private List<Weather> weather;
    private TemperatureData temperatureData;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    @JsonProperty("main")
    public TemperatureData getTemperatureData() {
        return temperatureData;
    }

    public void setTemperatureData(TemperatureData temperatureData) {
        this.temperatureData = temperatureData;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "id=" + id +
                ", location=" + location +
                ", actualWeather=" + weather.get(0).getDescription() +
                ", temperature=" + temperatureData.getTemperature() +
                "}";
    }
}
