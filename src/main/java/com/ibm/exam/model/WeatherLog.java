package com.ibm.exam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import java.util.Date;

@Entity
public class WeatherLog {

    @Id
    @GeneratedValue
    private long id;
    private String responseId;
    private String location;
    private String actualWeather;
    private String temperature;
    private Date dTimeInserted;

    public WeatherLog() {

    }

    public WeatherLog(String location, String actualWeather, String temperature) {
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Date getdTimeInserted() {
        return dTimeInserted;
    }

    public void setdTimeInserted(Date dTimeInserted) {
        this.dTimeInserted = dTimeInserted;
    }

    @PrePersist
    public void onCreate() {
        Date dateNow = new Date();

        if (dTimeInserted == null) {
            dTimeInserted = dateNow;
        }

        if (responseId == null) {
            responseId = location.substring(0, 3) + dateNow.getTime();
        }
    }
}
