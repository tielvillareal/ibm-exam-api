package com.ibm.exam.controller;

import com.ibm.exam.model.ActionResponse;
import com.ibm.exam.model.WeatherData;
import com.ibm.exam.model.WeatherLog;
import com.ibm.exam.repository.WeatherLogRepository;
import com.ibm.exam.service.WeatherApiService;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class WeatherController {

    @Autowired
    private WeatherApiService weatherApiService;
    @Autowired
    private WeatherLogRepository weatherLogRepository;

    private final String[] locations = {"London", "Prague", "San Francisco"};
    private Set<WeatherData> weatherDataSet = new LinkedHashSet<>();

    @GetMapping("/get-weather")
    @ResponseBody
    public List<WeatherData> getWeather() {
        List<WeatherData> weatherDataList = new ArrayList<>();

        for (String loc: locations) {
            WeatherData weatherData = weatherApiService.getWeatherData(loc);

            weatherDataList.add(weatherData);
            weatherDataSet.add(weatherData);
        }

        return weatherDataList;
    }

    @PostMapping("/log-weather")
    @ResponseBody
    public ActionResponse logWeather() {
        int ctr = 0;
        LinkedList<WeatherData> weatherDataList = new LinkedList<>(weatherDataSet);
        Iterator<WeatherData> weatherDataIterator = weatherDataList.descendingIterator();

        while (weatherDataIterator.hasNext()) {
            if (ctr > 4) break;
            WeatherData weatherData = weatherDataIterator.next();

            weatherLogRepository.save(new WeatherLog(weatherData.getLocation(), weatherData.getWeather().get(0).getDescription(), weatherData.getTemperatureData().getTemperature() + ""));
            ctr++;
        }

        return new ActionResponse("001", "Operation Successful");
    }
}
